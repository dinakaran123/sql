package model;

public class database_name {
	String message;
	String[] filename;
	String[] sheetname;
	public String[] getSheetname() {
		return sheetname;
	}

	public void setSheetname(String[] sheetname) {
		this.sheetname = sheetname;
	}

	public String[] getFilename() {
		return filename;
	}

	public void setFilename(String[] filename) {
		this.filename = filename;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
