package sqlquery1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import model.database_name;

/**
 * Servlet implementation class openDatabase
 */
@WebServlet("/openDatabase")
public class openDatabase extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public openDatabase() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Inside database");
		BufferedReader br=new BufferedReader(new InputStreamReader(request.getInputStream()));
		String json="";
		if(br!=null)
		{
			 json=br.readLine();
			System.out.println(json);
		}
		String location="/home/local/ZOHOCORP/kavin-9181/Documents/"+json+".xls";
		File file=new File(location);
		try {
			Workbook workbook= Workbook.getWorkbook(file);
			String[] sheetnames=workbook.getSheetNames();
			System.out.println(sheetnames.length);
			database_name d =new database_name();
			if(sheetnames.length==0)
			{
				d.setMessage("Failure");
			}
			else
			{
			d.setSheetname(sheetnames);
			}
			PrintWriter out=response.getWriter();
			Gson gson=new Gson();
			//d.setFilename(list);
			String jsonstring= gson.toJson(d);
			System.out.println(jsonstring);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			out.print(jsonstring);
			System.out.println(jsonstring);
			out.flush();
			
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
