package sqlquery1;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileUtils;

import com.google.gson.Gson;

import java.io.File;
import java.io.FilenameFilter;

import jxl.Workbook;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import model.database_name;
/**
 * Servlet implementation class Create_table
 */
@WebServlet("/Create_table")
public class Create_table extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Create_table() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	//	System.out.println("create table");
		String filedir="/home/local/ZOHOCORP/kavin-9181/Documents/";
		String fileext=".xls";
		GenericExtFilter filter = new GenericExtFilter(fileext);

		File dir=new File(filedir);
		
		if(dir.isDirectory()==false)
		{
			System.out.println("Directory not present");
		}
		String[] list=dir.list(filter);
	//	System.out.println(list.length);
		PrintWriter out=response.getWriter();
		Gson gson=new Gson();
		database_name d =new database_name();
		d.setFilename(list);
		String jsonstring= gson.toJson(d);
		System.out.println(jsonstring);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonstring);
		out.flush();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		doGet(request, response);
		
	}
	public class GenericExtFilter implements FilenameFilter {

		private String ext;

		public GenericExtFilter(String ext) {
			this.ext = ext;
		}

		public boolean accept(File dir, String name) {
			return (name.endsWith(ext));
		}

	}}


